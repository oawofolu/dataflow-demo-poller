package gov.luckyspells.canary.poller;

import java.io.InputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

import gov.luckyspells.canary.poller.service.PollerService;
import gov.luckyspells.canary.poller.service.UtilityService;

@EnableBinding(Source.class)
public class CsvPoller {
	
	@Autowired PollerService pollerService;
	@Autowired UtilityService utilityService;
	
	private static Logger LOG = LoggerFactory.getLogger(CsvPoller.class);
	
	@ServiceActivator(inputChannel = "splitter-data.splitter-data.errors")
	public void error(Message<?> message) {
		LOG.info("Error associated with message: " + message.getPayload());
	}

	@Bean
	@InboundChannelAdapter( 
			poller = @Poller(fixedDelay = "30000"),
			value = Source.OUTPUT)
	public MessageSource<String> tryPoll() {
		
		return () -> {
		
			InputStream fileStream = null;
			
			String message = "";
			
			try {
				
				LOG.info("Calling tryPoll...");
				
				// check if files exist
				List<String> files = pollerService.listFiles();
				
				if (!files.isEmpty()) {
					// get the first file
					String filename = files.get(0);
					LOG.info("File to be parsed: " + filename);
					
					// get the content associated with the filename
					fileStream = pollerService.getFile(filename);
					message = utilityService.getFileContent(filename, fileStream);
					
					// delete the file from the bucket
					pollerService.removeFile(filename);
					
				} 
				
			} catch (Exception ex) {
				throw new RuntimeException(ex);
			} finally {
				utilityService.closeQuietly(fileStream);
			}
				
			return MessageBuilder.withPayload(message).build();
		};
	}
}
