package gov.luckyspells.canary.poller.service;

import java.io.InputStream;
import java.util.List;

public interface PollerService {
	public List<String> listFiles() throws Exception;
	public InputStream getFile(String filename) throws Exception;
	public boolean removeFile(String filename) throws Exception;
}
