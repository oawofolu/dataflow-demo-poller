package gov.luckyspells.canary.poller.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Random;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

/**
 * 
 * TODO: This should be a separate library/module.
 *
 */
@Service
public class UtilityService {
	public File getFile(String filename, InputStream inputStream) {
		
		File file = null;
		try {
			String tempFileName = filename + new Random().nextInt();
			file = File.createTempFile(tempFileName, ".tmp");
		} catch (Exception ex) {
			throw new RuntimeException(
					String.format(
							"Could not create temp file: %s.tmp", filename));
		}
		try(OutputStream outputStream = new FileOutputStream(file)) {
			IOUtils.copy(inputStream, outputStream);
		} catch(Exception ex) {
			System.out.println("Could not convert file from inputstream");
			// TODO: Do not swallow exceptions!!!
		}
		return file;
	}
	
	public String getFileContent(String filename, InputStream inputStream) {
		String content = null;
		try {
			content = IOUtils.toString(inputStream,Charset.defaultCharset());
		} catch (Exception ex) {
			System.out.println("Could not convert file from inputstream");
			// TODO: Do not swallow exceptions!!!
		}
		
		return content;
	}
	
	public void closeQuietly(InputStream inputStream) {
		try {
			if (inputStream!=null) inputStream.close();
		} catch( Exception ex) {}
	}
}
