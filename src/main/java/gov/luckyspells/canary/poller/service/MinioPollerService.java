package gov.luckyspells.canary.poller.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import io.minio.MinioClient;
import io.minio.Result;
import io.minio.errors.MinioException;
import io.minio.messages.Item;

@Service
public class MinioPollerService implements PollerService {
	
	@Value("#{environment.s3_url}") private String s3_url;
	@Value("#{environment.s3_accesskey}") private String s3_accesskey;
	@Value("#{environment.s3_secretkey}") private String s3_secretkey;
	@Value("#{environment.s3_bucket}") private String s3_bucket;
	
	private static Logger LOG = LoggerFactory.getLogger(MinioPollerService.class);

	@Override
	public List<String> listFiles() throws Exception{
		List<String> fileNames = new ArrayList<>();
		
		try {
	      MinioClient minioClient = 
	    		  new MinioClient(s3_url, s3_accesskey,
	                                                s3_secretkey);

	      boolean found = minioClient.bucketExists(s3_bucket);
	      if (found) {
	        
	        Iterable<Result<Item>> myObjects = minioClient.listObjects(s3_bucket);
	        fileNames = new ArrayList<>();
	        for (Result<Item> result : myObjects) {
	          Item item = result.get();
	          fileNames.add(item.objectName());
	        }
	      } else {
	        throw new RuntimeException(s3_bucket+" does not exist");
	      }
	    } catch (MinioException e) {
	      throw new RuntimeException("Error occurred: " + e);
	    }
		
		return fileNames;
	}

	@Override
	public InputStream getFile(String filename) throws Exception{
		MinioClient minioClient = new MinioClient(s3_url, s3_accesskey,
                s3_secretkey);
		InputStream stream = null;
		
		try {
			minioClient.statObject(s3_bucket, filename);
			stream = minioClient.getObject(s3_bucket, filename);
		} catch(Exception e) {
			throw new RuntimeException(String.format("%s:%s does not exist",s3_bucket,filename));
		}
		return stream;
	}

	@Override
	public boolean removeFile(String filename) throws Exception {
		MinioClient minioClient = new MinioClient(s3_url, s3_accesskey,
                s3_secretkey);
		
		try {
			minioClient.removeObject(s3_bucket, filename);
		} catch(Exception e) {
			LOG.info(String.format("Could not delete %s from %s: %s", 
							filename, s3_bucket, e.getMessage())) ;
			return false;
		}
		return true;
	}

}
